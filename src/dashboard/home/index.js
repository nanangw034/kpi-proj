import React from "react";
import Header from "../header";
import { Row, Col } from "antd";

class HomePage extends React.Component {
  render() {
    return (
      <Row>
        <Header />
      </Row>
    );
  }
}

export default HomePage;
